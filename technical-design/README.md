# Part 2: (technical design)

Let’s say that we have plot all the scooters in Singapore on a map (hundreds of thousands) for our operations team to understand where all the scooters currently are. Please describe how you would do that, focusing on usability, performance and scalability. You can focus more on the front end, the backend or discuss both equally in your answer. (~1 page should be fine, but feel free to use more space if needed.)

### Front End design

#### Assumption

1.  No usecase of display all scooters in the system (hundreds of thousands) at once
1.  Scope by state
1.  Scope by road
1.  Scope by street
1.  Have 2 modes of display

- Single request mode
- Live mode

As assumption above

1. Client will select scooter by sending the upper left and lower right latitude and longitude of the map screen to fetch location of scooter data. But the server will return only data of location with limit amount of item return but separate the count amount of the whole location within upper left and lower right location such as the length of data return maybe equal to 2000 items but the count amount return 200000 so the client will recognize that the upper and lower area are too large then display alert to the user to zoom the map in or select by other criteria such as fetch by state or by city or by street and fetching new data

## Back End Design

1. Will use microservice which following services

- Scooter location upsert service
- Scooter location fetch service

## Back End Tropology

![alt scooterTopology](ScooterToplogy.png "Topology")

1. Scooter Location upsert service

- APIs server
- Geo Location Data source (MongoDB 1)
- CDC (Debezium MongDb connector)

2. Operation's Scooter location fetch service

- APIs server with WebSocket
- Geo Location Data source (MongoDB 2)
- Kafka Consumer

#### Geo Location Data Source for Operation Service

1. geo_location collection (collection to collect geo location of state, city, street building)
1. scooter_location collection (collection to collect scooter current location)

#### Fetch Scooter Location Scenario

1. Once operation client in the live mode connect to WebSocket service the service will collect the current upper left and lower right latitude and longitude (Boundary data) of the displayed map on the screen as body data and once the client zoom in or out or pan the map then the client will also send the current boundary data (not every action need to optimize how often or when to send) via this webSocket and the service will recognize the client by client uuid along with current boundary data
1. For first connection of the client APIs server will return current location of each scooter within the boundary data with limited item of data eventually there are more data of scooter location within the boundary
1. Once Kafka Consumer process a change of data according to Kafka message into Local Data Source (MongoDB 2) after that notify the WebSocket service then the WebSocket service will consider which clients to send the change of data to depend on the client's current boundary and the data receive from Kafka message
1. Once the client receive the changed data via webSocket then the client be able to move or change status of marker belong to changed displayed on the map.
