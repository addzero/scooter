FROM ubuntu:latest as builder
RUN apt-get update && apt-get install -yq curl file git unzip xz-utils zip && apt-get clean
# Set up new user
RUN useradd -ms /bin/bash scooter

# Copy the app files to the container
COPY ./scooter-client /home/scooter/bin/app
RUN chown -R scooter:scooter /home/scooter/bin/app

USER scooter

WORKDIR /home/scooter

# Clone the flutter repo
RUN git clone https://github.com/flutter/flutter.git /home/scooter/flutter

# Set flutter path
ENV PATH="/home/scooter/flutter/bin:/home/scooter/flutter/bin/cache/dart-sdk/bin:${PATH}"

# Enable flutter web
RUN flutter channel master
RUN flutter upgrade
RUN flutter config --enable-web
RUN flutter pub global activate webdev

# Run flutter doctor
RUN flutter doctor -v

# Set the working directory to the app files within the container
WORKDIR /home/scooter/bin/app

# Get App Dependencies
RUN flutter pub get

# Build the app for the web
RUN flutter build web

FROM nginx:latest

COPY --from=Builder /home/scooter/bin/app/build/web /usr/share/nginx/html
