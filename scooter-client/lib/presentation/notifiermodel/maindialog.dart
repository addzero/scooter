import 'package:flutter/material.dart';

class MainEventDataModel with ChangeNotifier {
  String _title;
  String _message;
  String _event;

  String get message => _message;
  String get title => _title;
  String get event => _event;

  void assignMainEventData(String event, String title, String message) async {
    _title = title;
    _event = event;
    _message = message;
    notifyListeners();
  }
}
