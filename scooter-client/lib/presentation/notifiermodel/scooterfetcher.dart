import 'package:get_it/get_it.dart';
import "package:latlong/latlong.dart";
import 'package:flutter/material.dart';
import 'package:scooter/core/error/failure.dart';
import 'package:scooter/entity/scooterlocation.dart';
import "dart:math";
import 'package:scooter/usecase/data/scooterlocationfetch.dart';
import 'package:dartz/dartz.dart';

class ScooterFetcherModel with ChangeNotifier {
  double _radius = 1000;
  LatLng _center = LatLng(42, 100);
  double _zoom = 16;
  int _limit = 20;
  List<ScooterLocation> _scooterLocations;

  double get radius => _radius;
  double get zoom => _zoom;
  LatLng get center => _center;
  int get limit => _limit;
  List<ScooterLocation> get scooterLocations => _scooterLocations;

  void assignFetcherData(
      BuildContext context, double radius, LatLng center, int limit) async {
    _center = center;
    _radius = radius;
    _limit = limit;
    if (radius > 0) {
      double radiusElevated = radius + radius / 2;
      double scale = radiusElevated / 500;
      _zoom = 16.5 - log(scale) / log(2);
      _zoom = _zoom > 15 ? 15 : _zoom < 0.6 ? 0.6 : _zoom;
    }

    ScooterLocationFetchUseCaseParams param = ScooterLocationFetchUseCaseParams(
      center: _center,
      radius: _radius,
      limit: _limit,
    );

    final Either<Failure, List<ScooterLocation>> result =
        await GetIt.instance<ScooterLocationFetchUseCase>().call(param);

    void handleFailure(ServerFailure failure) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("error"),
            content: Text(failure.message),
            actions: <Widget>[
              new OutlinedButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    result.fold(
        (l) => {handleFailure(l)},
        (r) => {_scooterLocations = r});

    notifyListeners();
  }
}
