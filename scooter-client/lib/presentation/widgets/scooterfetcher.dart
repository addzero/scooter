import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import "package:latlong/latlong.dart";
import 'package:provider/provider.dart';
import 'package:scooter/presentation/notifiermodel/scooterfetcher.dart';

class ScooterFetcher extends StatefulWidget {
  ScooterFetcher({Key key}) : super(key: key);

  @override
  _ScooterFetcherState createState() => _ScooterFetcherState();
}

class _ScooterFetcherState extends State<ScooterFetcher> {
  final latitudeController = TextEditingController();
  final longitudeController = TextEditingController();
  final radiusController = TextEditingController()..text = '1000';
  final limitController = TextEditingController()..text = '20';

  String numberValidator(String value, double max, double min, String attribute, bool isInteger) {
    if(value == null) {
      return null;
    }
    final n = num.tryParse(value);
    if(n == null) {
      return 'is not a valid $attribute';
    }

    if (n > max) {
      return 'cannot be greater than $max';
    }

    if (n < min) {
      return 'cannot be less than $min';
    }

    if (isInteger && !(n is int)) {
      return 'must be integer';
    }
    return null;
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    assignScooterFetchParameters() {
      double latitude = double.parse(
          latitudeController.text == "" ? "0" : latitudeController.text);
      double longitude = double.parse(
          longitudeController.text == "" ? "0" : longitudeController.text);
      double radius = double.parse(
          radiusController.text == "" ? "1000" : radiusController.text);
      int limit = int.parse(
          limitController.text == "" ? "20" : limitController.text);
      var fetcher = context.read<ScooterFetcherModel>();
      fetcher.assignFetcherData(context, radius, LatLng(latitude, longitude), limit);
    }

    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Flexible(
                        child: TextFormField(
                            validator: (value) { return numberValidator(value, 90, -90, 'latitude', false);},
                            keyboardType: TextInputType.number,
                            controller: latitudeController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'latitude',
                            ))),

                    Flexible(
                        child: TextFormField(
                          validator: (value) { return numberValidator(value, 180, -180, 'longitude', false);},
                      controller: longitudeController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'longitude',
                      ),
                    )),
                  ],
                ),
                Row(
                  children: [
                    Flexible(
                        child: TextFormField(
                          validator: (value) { return numberValidator(value, 10000000000, 100, 'radius', false);},
                      controller: radiusController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'radius (m)',
                      ),
                    )),
                    Flexible(
                        child: TextFormField(
                          validator: (value) { return numberValidator(value, 10000, 1, 'limit', true);},
                          controller: limitController,
                          keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'limit',
                          ),
                        )),
                  ],
                ),
                SizedBox(
                    width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              assignScooterFetchParameters();
                              }
                            },
                          child: Text('Submit'),
                        )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
