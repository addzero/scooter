import "package:latlong/latlong.dart";
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

class ScooterMap extends StatefulWidget {
  final LatLng center;
  final double zoom;
  final List<Marker> marker;
  ScooterMap({Key key, this.center, this.zoom = 13, this.marker})
      : super(key: key);
  @override
  _ScooterMapState createState() => _ScooterMapState();
}

class _ScooterMapState extends State<ScooterMap> {
  final MapController mapController = MapController();

  @override
  Widget build(BuildContext context) {
    mapController.onReady.then((result) {
      mapController.move(widget.center, widget.zoom);
    });

    return FlutterMap(
      mapController: mapController,
      options: MapOptions(
        center: widget.center,
        zoom: widget.zoom,
      ),
      layers: [
        TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']),
        MarkerLayerOptions(
          markers: widget.marker,
        ),
      ],
    );
  }
}
