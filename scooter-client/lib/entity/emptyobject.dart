import 'package:equatable/equatable.dart';

class EmptyObjectEntity extends Equatable {
  @override
  List<Object> get props => [];
}
