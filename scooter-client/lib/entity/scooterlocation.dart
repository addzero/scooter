import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import "package:latlong/latlong.dart";

class Location {
  final List<double> coordinates;
  final String type;

  Location({this.coordinates, this.type});
}

class ScooterLocation extends Equatable {
  final int scooterId;
  final Location location;
  final int riderId;
  ScooterLocation(
      {@required this.scooterId, @required this.location, this.riderId});

  @override
  List<Object> get props => [scooterId, location, riderId];
}

class ScooterLocationFetchUseCaseParams extends Equatable {
  final double radius;
  final LatLng center;
  final int limit;

  ScooterLocationFetchUseCaseParams({this.center, this.radius, this.limit});

  @override
  List<Object> get props => [center, radius, limit];
}
