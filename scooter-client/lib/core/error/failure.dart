import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  Failure([List properties = const <dynamic>[]]);
}

class CacheFailure extends Failure {
  List<Object> get props => [];
}

class LibraryFailure extends Failure {
  List<Object> get props => [];
}

class ServerFailure extends Failure {
  final String message;
  final int code;
  ServerFailure(
      {this.message, this.code});

  @override
  List<Object> get props => [message, code];
}
