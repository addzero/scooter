import 'package:get_it/get_it.dart';
import 'package:scooter/repository/data/scooterlocation.dart';
import 'package:scooter/repository/scooterlocation.dart';
import 'package:scooter/usecase/data/scooterlocationfetch.dart';

final sl = GetIt.instance;

Future<void> setUp() async {
  // repository
  sl.registerLazySingleton<ScooterLocationRepository>(
      () => ScooterLocationRepositoryImp());

  // usecase
  sl.registerLazySingleton<ScooterLocationFetchUseCase>(
      () => ScooterLocationFetchUseCase(
            scooterLocationRepository: sl(),
          ));
}
