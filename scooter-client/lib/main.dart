import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:scooter/entity/scooterlocation.dart';
import 'package:scooter/presentation/notifiermodel/maindialog.dart';
import 'package:scooter/presentation/notifiermodel/scooterfetcher.dart';
import 'package:scooter/presentation/widgets/map.dart';
import 'package:provider/provider.dart';
import 'package:scooter/presentation/widgets/scooterfetcher.dart';
import 'package:scooter/service_locator.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import "package:latlong/latlong.dart";

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setUp();
  FlavorConfig(
    name: "LOCAL",
    variables: {
      "counter": 5,
      "baseUrl": "localhost:8989",
    },
  );
  runApp(Scooter());
}

class Scooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Scooter assignment',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: ScooterHomePage(title: 'Fetch scooter'),
    );
  }
}

class ScooterHomePage extends StatefulWidget {
  ScooterHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ScooterHomePageState createState() => _ScooterHomePageState();
}

class _ScooterHomePageState extends State<ScooterHomePage> {
  List<Marker> listMarker(LatLng center, List<ScooterLocation> data, double zoom) {
    Marker centerMarker = Marker(
      point: LatLng(
          center.latitude, center.longitude),
      width: 40,
      height: 40,
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_searching,
          color: Colors.pink,
          size: 24.0,
          semanticLabel: 'Text to announce in accessibility modes',
        ),
      ),
    );
    if (data == null) return [centerMarker];

    List<Marker> list = data.map((e) => Marker(
      point: LatLng(
          e.location.coordinates[1], e.location.coordinates[0]),
      width: 40,
      height: 40,
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on_outlined,
          color: Colors.pink,
          size: 24.0,
          semanticLabel: 'Text to announce in accessibility modes',
        ),
      ),
    )).toList();
    list.insert(0, centerMarker);
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ChangeNotifierProvider(
            create: (context) => MainEventDataModel(),
            child: Consumer<MainEventDataModel>(
              builder: (context, mainEventData, _) {
                return Padding(
                    padding: EdgeInsets.all(8.0),
                      child: ChangeNotifierProvider(
                        create: (context) => ScooterFetcherModel(),
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                              child: ScooterFetcher(),
                            ),
                            Flexible(
                              child: Consumer<ScooterFetcherModel>(
                                  builder: (context, fetch, child) => ScooterMap(
                                        center: fetch.center,
                                        zoom: fetch.zoom,
                                        marker: listMarker(
                                          fetch.center,
                                            fetch.scooterLocations, fetch.zoom),
                                      )),
                            ),
                          ],
                        ),
                      ),
            );
              })));

  }
}
