import 'package:dartz/dartz.dart';
import 'package:scooter/core/error/failure.dart';
import 'package:scooter/entity/scooterlocation.dart';
import "package:latlong/latlong.dart";

abstract class ScooterLocationRepository {
  Future<Either<Failure, List<ScooterLocation>>> fetchByRadius(
      LatLng location, double radius, int limit);
}
