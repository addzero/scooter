import 'package:scooter/repository/model/scooterlocation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'scooterlocationfetchresponse.g.dart';

@JsonSerializable(createToJson: false)
class ScooterLocationFetchResponse {
  final List<ScooterLocationModel> data;
  final int length;
  final String message;

  ScooterLocationFetchResponse({this.length, this.data, this.message});

  factory ScooterLocationFetchResponse.fromJson(Map<String, dynamic> json) =>
      _$ScooterLocationFetchResponseFromJson(json);
}
