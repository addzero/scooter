// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scooterlocationfetchresponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScooterLocationFetchResponse _$ScooterLocationFetchResponseFromJson(
    Map<String, dynamic> json) {
  return ScooterLocationFetchResponse(
    length: json['length'] as int,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : ScooterLocationModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    message: json['message'] as String,
  );
}
