// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'scooterlocation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ScooterLocationModel _$ScooterLocationModelFromJson(Map<String, dynamic> json) {
  return ScooterLocationModel(
    json['scooterId'] as int,
    json['location'] == null
        ? null
        : LocationModel.fromJson(json['location'] as Map<String, dynamic>),
  );
}
