import 'package:scooter/entity/scooterlocation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:scooter/repository/model/location.dart';

part 'scooterlocation.g.dart';

@JsonSerializable(createToJson: false)
class ScooterLocationModel extends ScooterLocation {
  final int scooterId;
  final LocationModel location;

  ScooterLocationModel(this.scooterId, this.location)
      : super(scooterId: scooterId, location: location);

  factory ScooterLocationModel.fromJson(Map<String, dynamic> json) =>
      _$ScooterLocationModelFromJson(json);
}
