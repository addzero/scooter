import 'package:scooter/entity/scooterlocation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'location.g.dart';

@JsonSerializable(explicitToJson: true)
class LocationModel extends Location {
  final List<double> coordinates;
  final String type;

  LocationModel({this.coordinates, this.type});

  factory LocationModel.fromJson(Map<String, dynamic> json) =>
      _$LocationModelFromJson(json);
}
