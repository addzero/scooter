# Beam Assignment

## This project contain 4 parts

1. [Scooter apis service](scooter-apis)
1. [Scooter client](scooter-client)
1. [Document](document)
1. [Technical Design](technical-design)

### Scooter apis service

- Using [GOLANG](https://golang.org/) with [echo web framework](https://echo.labstack.com/)
- Connect to [MongDB](https://www.mongodb.com/) using mongodb driver

### Scooter client

- Using [Flutter](https://flutter.dev/) for cross platform client such as iOS, android or Web

### Document

- Attached postman collection of 2 APIs endpoint provide for this assignment

### Technical design

- Will be a 2nd part of the assignment which discuss about how to display all of hundreds of thousands scooters current location and status.

## Prerequisite

1. [Docker](https://www.docker.com)
1. [Docker compose](https://docs.docker.com/compose/)

## Start services

1. Just run ` docker-compose up` on your command line terminal

## Manage scooter location data

To manage scooter location data import [Postman collection](document/postman/Beam-Scooter.postman_collection.json) attach in this project which contain 2 APIs endpoints

1. [Scooter location upsert](#upsert)
1. [Scooter location fetch](#fetch)

## <a name='upsert'>Scooter location upsert</a>

Use for insert or update scooter location data. This API can be call via command line by

```sh
curl --location --request POST 'localhost:8989/api/v1/scooter/:scooter_id/location' \
--header 'Content-Type: application/json' \
--data-raw '{
    "latitude": {{latitude}},
    "longitude": {{longitude}}
}'
```

Which be able to input

- latitude as any value between -90 to 90
- longitude as any value between -180 to 180

### <a name='fetch'>Scooter location fetch</a>

Use for fetch scooter location data.

```sh
curl --location --request GET 'localhost:8989/api/v1/scooter/location?latitude={{latitude}}&longitude={{longitude}}&radius={{radius}}&limit={{limit}}&offset={{offset}}'
```

Which be able to input

- latitude as any value between -90 to 90
- longitude as any value between -180 to 180
- radius in metre unit
- limit the number of location related input latitude, longitude and radius to be fetch
- offset the number of skip location related input latitude, longitude and radius to be fetch

## Usage

1. Open your favorite web browser and goto [locahost:8080](http://location:8080) then you will see a simple web page
1. Enter some data in the input text field to fetch scooter location data then the marker of near by scooter location of your input will be display on the displayed map
1. Need to add more scooter location then import [postman collection](document/postman/Beam-Scooter.postman_collection.json) into your postman and call `scooter-location-upsert` with your desire data as much as you need.
