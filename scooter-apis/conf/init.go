package conf

import (
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"io/ioutil"
	"log"
	"os"
)

var configure map[string]interface{}

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	path, exist := os.LookupEnv("CONFIGURATION_FILE_PATH")
	if !exist {
		log.Fatal("configuration file not found")
	}

	file, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("Config file error: %v\n", err)
		os.Exit(1)
	}

	if err := json.Unmarshal(file, &configure); err != nil {
		log.Fatal(err)
	}
	return
}

func ReadConfigure(key string, require ...bool) interface{} {
	confValue, confOK := configure[key]
	if !confOK && len(require) > 0 && require[0] {
		log.Fatal(fmt.Sprintf("%s not found", key))
	}
	return confValue
}
