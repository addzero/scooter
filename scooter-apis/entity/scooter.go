package entity

import "time"

type Scooter struct {
	ID uint
}

type Rider struct {
	ID uint
}

type ScooterLocationUpsert struct {
	Scooter  Scooter
	Rider    *Rider
	Location GeoPoint
}

type Location struct {
	Coordinates []float64
}

type ScooterLocation struct {
	ScooterID uint
	RiderID   *uint
	Location  Location
	CreatedAt time.Time
	UpdatedAt time.Time
	Distance  float64
}
