package entity

type GeoPoint struct {
	Latitude  float64
	Longitude float64
}
