package entity

type Sort struct {
	Key   string
	Order string
}

type CommonFetch struct {
	Limit     uint
	OffSet    uint
	Sort      []Sort
	NeedCount bool
}
