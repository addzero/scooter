module github.com/addzero/beam/scooter

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.25 // indirect
	github.com/go-playground/validator/v10 v10.5.0
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/jinzhu/copier v0.3.0
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.12.1 // indirect
	github.com/labstack/echo/v4 v4.2.2
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/pborman/uuid v1.2.1
	github.com/stretchr/testify v1.6.1
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	go.mongodb.org/mongo-driver v1.5.1
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/net v0.0.0-20210423184538-5f58ad60dda6
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210423185535-09eb48e85fd7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
