package core

import (
	"context"

	"github.com/addzero/beam/scooter/konst"
	"github.com/pborman/uuid"
)

func NewRequestIDContext(ctx context.Context) context.Context {
	scooterCtxValue := map[string]interface{}{
		konst.RequestID: uuid.New(),
	}
	return context.WithValue(ctx, konst.Scooter, scooterCtxValue)
}

func GetRequestID(ctx context.Context) (string, error) {
	id, ok := ctx.Value(konst.RequestID).(string)
	if !ok {
		return "", konst.RequestIdNotFound
	}
	return id, nil
}
