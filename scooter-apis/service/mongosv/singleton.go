package mongosv

import (
	"log"

	"github.com/addzero/beam/scooter/conf"
	"github.com/addzero/beam/scooter/konst"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/context"
)

type mongoSingleton struct{}

func (s mongoSingleton) GetClient() *mongo.Client {
	client := *mongoClient
	return &client
}

var singleton mongoSingleton

func init() {
	singleton = mongoSingleton{}
	initMongoClient()
}

var mongoClient *mongo.Client

func createIndexes(ctx context.Context) {
	indexModel := mongo.IndexModel{
		Keys: bson.D{
			{Key: "location", Value: "2dsphere"},
		},
	}
	dbName, _ := conf.ReadConfigure(konst.MongoDbName, true).(string)
	mongoClient.Database(dbName).Collection("scooter_location").Indexes().CreateOne(ctx, indexModel)
}

func initMongoClient() {
	defer func() {
		if r := recover(); r != nil {
			log.Println("recover in ", r)
		}
	}()
	if mongoClient == nil {
		ctx := context.TODO()
		mongoConf, _ := conf.ReadConfigure(konst.MongoDbConfig, true).(string)
		clientOptions := options.Client().ApplyURI(mongoConf)
		newClient, err := mongo.Connect(ctx, clientOptions)
		if err != nil {
			log.Panic(err)
		}
		mongoClient = newClient

		createIndexes(ctx)
	}
}

func (m mongoSingleton) GetCollection(collectionName string) *mongo.Collection {
	dbName, _ := conf.ReadConfigure(konst.MongoDbName, true).(string)
	return mongoClient.Database(dbName).Collection(collectionName)
}

func Instance() mongoSingleton {
	return singleton
}
