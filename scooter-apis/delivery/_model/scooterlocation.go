package _model

import "time"

type Location struct {
	Coordinates []float64 `json:"coordinates" bson:"coordinates"`
}

type ScooterLocation struct {
	ScooterID uint `json:"scooter_id" bson:"scooter_id"`
	RiderID   *uint `json:"rider_id,omitempty" bson:"rider_id,omitempty"`
	Location Location `json:"location" bson:"location"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
	Distance float64 `json:"distance" bson:"distance"`
}