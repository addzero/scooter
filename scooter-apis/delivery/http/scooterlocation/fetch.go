package scooterlocation

import (
	"context"
	"net/http"
	"strconv"

	"github.com/addzero/beam/scooter/core"
	"github.com/addzero/beam/scooter/delivery/_model"
	deliveryhttp "github.com/addzero/beam/scooter/delivery/http"
	"github.com/addzero/beam/scooter/entity"
	"github.com/addzero/beam/scooter/konst"
	"github.com/addzero/beam/scooter/validator"
	"github.com/jinzhu/copier"
	"github.com/labstack/echo/v4"
)

func (s *scooterLocationHandler) Fetch(ctx echo.Context) error {
	var lim, off, rad, lat, lon = "100", "0", "0", "0", "0"
	li := ctx.QueryParam(konst.Limit)
	if li != "" {
		lim = li
	}
	limit, limitErr := strconv.ParseUint(lim, 10, 64)
	if limitErr != nil {
		return ctx.JSON(http.StatusBadRequest, deliveryhttp.NewErrorResponse("DB00", limitErr))
	}

	of := ctx.QueryParam(konst.Offset)
	if of != "" {
		off = of
	}
	offset, offsetErr := strconv.ParseUint(off, 10, 64)
	if offsetErr != nil {
		return ctx.JSON(http.StatusBadRequest, deliveryhttp.NewErrorResponse("DB01", offsetErr))
	}

	ra := ctx.QueryParam(konst.Radius)
	if ra != "" {
		rad = ra
	}
	radius, radiusErr := strconv.ParseFloat(rad, 64)
	if radiusErr != nil {
		return ctx.JSON(http.StatusBadRequest, deliveryhttp.NewErrorResponse("DB02", radiusErr))
	}

	la := ctx.QueryParam(konst.Latitude)
	if la != "" {
		lat = la
	}
	latitude, latitudeErr := strconv.ParseFloat(lat, 64)
	if latitudeErr != nil {
		return ctx.JSON(http.StatusBadRequest, deliveryhttp.NewErrorResponse("DB03", latitudeErr))
	}

	lo := ctx.QueryParam(konst.Longitude)
	if lo != "" {
		lon = lo
	}
	longitude, longitudeErr := strconv.ParseFloat(lon, 64)
	if longitudeErr != nil {
		return ctx.JSON(http.StatusBadRequest, deliveryhttp.NewErrorResponse("DB04", longitudeErr))
	}

	point := _model.GeoPoint{
		Latitude:  latitude,
		Longitude: longitude,
	}

	validateErr := validator.GetInstance().Struct(&point)
	if validateErr != nil {
		return ctx.JSON(http.StatusBadRequest, deliveryhttp.NewErrorResponse("DB05", validateErr))
	}

	commonFetch := entity.CommonFetch{
		Limit:     uint(limit),
		OffSet:    uint(offset),
		Sort:      []entity.Sort{{Key: konst.Radius, Order: konst.Desc}},
		NeedCount: true,
	}

	requestCtx := core.NewRequestIDContext(context.TODO())
	scooterLocations, length, fetchErr := s.scooterLocationFetchUsecase.FetchByRadius(
		requestCtx,
		entity.GeoPoint{
			Latitude:  latitude,
			Longitude: longitude,
		},
		radius,
		commonFetch)
	if fetchErr != nil {
		return ctx.JSON(http.StatusInternalServerError, deliveryhttp.NewErrorResponse("DB06", fetchErr))
	}

	var copyData []_model.ScooterLocation
	copyErr := copier.Copy(&copyData, scooterLocations)
	if copyErr != nil {
		return ctx.JSON(http.StatusInternalServerError, deliveryhttp.NewErrorResponse("DB07", copyErr))
	}

	return ctx.JSON(http.StatusOK, deliveryhttp.NewSuccessResponse(copyData, length))
}
