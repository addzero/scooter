package scooterlocation

import (
	"context"
	"net/http"
	"strconv"

	"github.com/addzero/beam/scooter/core"
	"github.com/addzero/beam/scooter/delivery/_model"
	http2 "github.com/addzero/beam/scooter/delivery/http"
	"github.com/addzero/beam/scooter/entity"
	"github.com/addzero/beam/scooter/konst"
	"github.com/addzero/beam/scooter/validator"
	"github.com/labstack/echo/v4"
)

func (s *scooterLocationHandler) Upsert(ctx echo.Context) error {
	scooterID := ctx.Param(konst.ScooterID)
	validateIdErr := validator.GetInstance().Var(scooterID, "required,numeric,min=1")
	if validateIdErr != nil {
		return ctx.JSON(http.StatusBadRequest, http2.NewErrorResponse("DA00", validateIdErr))
	}

	id, convErr := strconv.Atoi(scooterID)
	if convErr != nil {
		return ctx.JSON(http.StatusBadRequest, http2.NewErrorResponse("DA01", convErr))
	}

	validateIdErr = validator.GetInstance().Var(id, "min=1")
	if validateIdErr != nil {
		return ctx.JSON(http.StatusBadRequest, http2.NewErrorResponse("DA02", validateIdErr))
	}

	var point _model.GeoPoint
	if bindErr := ctx.Bind(&point); bindErr != nil {
		return ctx.JSON(http.StatusBadRequest, http2.NewErrorResponse("DA03", bindErr))
	}

	validateErr := validator.GetInstance().Struct(point)
	if validateErr != nil {
		return ctx.JSON(http.StatusBadRequest, http2.NewErrorResponse("DA04", validateErr))
	}

	requestCtx := core.NewRequestIDContext(context.TODO())
	upsertErr := s.scooterLocationUpsertUsecase.UpsertScooter(requestCtx, entity.ScooterLocationUpsert{
		Scooter: entity.Scooter{ID: uint(id)},
		Location: entity.GeoPoint{
			Latitude:  point.Latitude,
			Longitude: point.Longitude,
		},
	})
	if upsertErr != nil {
		return ctx.JSON(http.StatusInternalServerError, http2.NewErrorResponse("DA05", upsertErr))
	}

	return ctx.JSON(http.StatusOK, http2.NewSuccessResponse(nil, nil))
}
