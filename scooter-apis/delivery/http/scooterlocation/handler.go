package scooterlocation

import (
	"github.com/addzero/beam/scooter/usecase"
	"github.com/labstack/echo/v4"
)

type scooterLocationHandler struct {
	echo                         *echo.Echo
	scooterLocationUpsertUsecase usecase.ScooterLocationUpsert
	scooterLocationFetchUsecase  usecase.ScooterLocationFetch
}

type ScooterLocationHandlerParam struct {
	Echo                        *echo.Echo
	ScooterLocationUpserUsecase usecase.ScooterLocationUpsert
	ScooterLocationFetchUsecase usecase.ScooterLocationFetch
}

func NewScooterLocationHandler(param ScooterLocationHandlerParam) {
	scooterHdr := scooterLocationHandler{
		echo:                         param.Echo,
		scooterLocationUpsertUsecase: param.ScooterLocationUpserUsecase,
		scooterLocationFetchUsecase:  param.ScooterLocationFetchUsecase,
	}
	param.Echo.POST("/api/v1/scooter/:scooter_id/location", scooterHdr.Upsert)
	param.Echo.GET("/api/v1/scooter/location", scooterHdr.Fetch)
}
