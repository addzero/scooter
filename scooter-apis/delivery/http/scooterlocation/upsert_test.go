package scooterlocation

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/addzero/beam/scooter/usecase/mocks"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/mock"
)

func Test_scooterLocationHandler_Upsert(t *testing.T) {
	type args struct {
		id   string
		body string
	}
	type mocker struct {
		scooterLocationUpsertUsecaseError error
	}
	tests := []struct {
		name       string
		args       args
		mocker     mocker
		wantStatus int
	}{
		{
			name: "should fails if invalid json body",
			args: args{
				id:   "1",
				body: `{"latitude": -2.90892, "longitude": -44.45819`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "should fails if id is not numeric",
			args: args{
				id:   "A",
				body: `{"latitude": -2.90892, "longitude": -44.45819}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "should fails if id is minus numeric",
			args: args{
				id:   "-1",
				body: `{"latitude": -2.90892, "longitude": -44.45819}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "should fails if invalid latitude",
			args: args{
				id:   "1",
				body: `{"latitude": 91, "longitude": -44.45819}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "should fails if invalid longitude",
			args: args{
				id:   "1",
				body: `{"latitude": -2.90892, "longitude": 181}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "success with id 1 latitude longitude group 1",
			args: args{
				id:   "1",
				body: `{"latitude": -2.90892, "longitude": -44.45819}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "success with id 10000000000 latitude longitude group 1",
			args: args{
				id:   "1",
				body: `{"latitude": -2.90892, "longitude": -44.45819}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusOK,
		}, {
			name: "success with id 1 latitude longitude group 2",
			args: args{
				id:   "1",
				body: `{"latitude": 9.60929, "longitude": 144.27095}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "success with id 1 latitude longitude min",
			args: args{
				id:   "1",
				body: `{"latitude": -90, "longitude": -180}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "success with id 1 latitude min longitude max",
			args: args{
				id:   "1",
				body: `{"latitude": -90, "longitude": 180}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "success with id 1 latitude longitude max",
			args: args{
				id:   "1",
				body: `{"latitude": 90, "longitude": 180}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "success with id 1 latitude max longitude min",
			args: args{
				id:   "1",
				body: `{"latitude": 90, "longitude": -180}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusOK,
		},
		{
			name: "should fails with id 1 latitude above max longitude min",
			args: args{
				id:   "1",
				body: `{"latitude": 91, "longitude": -180}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "should fails with id 1 latitude max longitude above max",
			args: args{
				id:   "1",
				body: `{"latitude": 90, "longitude": 181}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "should fails with id 1 latitude min longitude less than min",
			args: args{
				id:   "1",
				body: `{"latitude": -90, "longitude": -181}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: nil,
			},
			wantStatus: http.StatusBadRequest,
		},
		{
			name: "should fails if scooter location upsert usecase error",
			args: args{
				id:   "1",
				body: `{"latitude": -2.90892, "longitude": -44.45819}`,
			},
			mocker: mocker{
				scooterLocationUpsertUsecaseError: fmt.Errorf("scooter location upsert error"),
			},
			wantStatus: http.StatusInternalServerError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := echo.New()
			upsertUsecase := new(mocks.ScooterLocationUpsert)
			upsertUsecase.On("UpsertScooter", mock.Anything, mock.Anything).Return(tt.mocker.scooterLocationUpsertUsecaseError)

			NewScooterLocationHandler(ScooterLocationHandlerParam{
				Echo:                        e,
				ScooterLocationUpserUsecase: upsertUsecase,
			})

			requestURL := fmt.Sprintf("/api/v1/scooter/%s/location", tt.args.id)
			req, _ := http.NewRequest("POST", requestURL, strings.NewReader(tt.args.body))
			req.Header.Set("content-type", "application/json")
			res := httptest.NewRecorder()
			e.ServeHTTP(res, req)

			if res.Code != tt.wantStatus {
				t.Errorf("Upsert() got http status = %v, wantStatus %v", res.Code, tt.wantStatus)
			}
		})
	}
}
