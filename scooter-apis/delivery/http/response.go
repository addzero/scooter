package http

type HttpResponse struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data,omitempty"`
	Message string      `json:"message,omitempty"`
	Code    string      `json:"code,omitempty"`
	Length  *uint       `json:"length,omitempty"`
}

func NewErrorResponse(code string, err error) HttpResponse {
	return HttpResponse{
		Success: false,
		Code:    code,
		Message: err.Error(),
	}
}

func NewSuccessResponse(data interface{}, length *uint) HttpResponse {
	return HttpResponse{
		Success: true,
		Data:    data,
		Length:  length,
	}
}
