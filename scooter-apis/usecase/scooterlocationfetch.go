package usecase

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
)

type ScooterLocationFetch interface {
	FetchByRadius(context.Context, entity.GeoPoint, float64, entity.CommonFetch) ([]entity.ScooterLocation, *uint, error)             // point, radius, offset, limit
	FetchByGeometry(context.Context, entity.GeoPoint, []entity.GeoPoint, entity.CommonFetch) ([]entity.ScooterLocation, *uint, error) // point, geometry, offset, limit
}
