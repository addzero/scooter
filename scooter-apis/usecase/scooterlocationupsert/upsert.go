package scooterlocationupsert

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
)

func (s scooterUpsertUsecase) UpsertScooter(ctx context.Context, data entity.ScooterLocationUpsert) error {
	return s.scooterRepository.Upsert(ctx, data)
}
