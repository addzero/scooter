package scooterlocationupsert

import (
	"context"
	"fmt"
	"testing"

	"github.com/addzero/beam/scooter/entity"
	"github.com/addzero/beam/scooter/repository/mocks"
	"github.com/stretchr/testify/mock"
)

func Test_scooterUpsertUsecase_UpsertScooter(t *testing.T) {
	type args struct {
		ctx  context.Context
		data entity.ScooterLocationUpsert
	}

	type mocker struct {
		scooterLocationMongoErr error
	}

	tests := []struct {
		name    string
		args    args
		mocker  mocker
		wantErr bool
	}{
		{
			name: "should fails if scooter location mongo upsert error",
			mocker: mocker{
				scooterLocationMongoErr: fmt.Errorf("mongo scooter upsert error"),
			},
			wantErr: true,
		},
		{
			name:    "should success if all upsert ok",
			mocker:  mocker{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mongoUpsertRepo := new(mocks.ScooterLocation)
			mongoUpsertRepo.On("Upsert", mock.Anything, mock.Anything).Return(tt.mocker.scooterLocationMongoErr)

			s := NewScooterUpsertUsecase(ScooterUpsertUsecaseParam{
				ScooterRepository: mongoUpsertRepo,
			})
			if err := s.UpsertScooter(tt.args.ctx, tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("UpsertScooter() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
