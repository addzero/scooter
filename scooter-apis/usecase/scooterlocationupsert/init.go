package scooterlocationupsert

import (
	"github.com/addzero/beam/scooter/repository"
	"github.com/addzero/beam/scooter/usecase"
)

type scooterUpsertUsecase struct {
	scooterRepository repository.ScooterLocation
}

type ScooterUpsertUsecaseParam struct {
	ScooterRepository repository.ScooterLocation
}

func NewScooterUpsertUsecase(param ScooterUpsertUsecaseParam) usecase.ScooterLocationUpsert {
	return &scooterUpsertUsecase{
		scooterRepository: param.ScooterRepository,
	}
}
