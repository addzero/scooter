package usecase

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
)

type ScooterLocationUpsert interface {
	UpsertScooter(context.Context, entity.ScooterLocationUpsert) error
}
