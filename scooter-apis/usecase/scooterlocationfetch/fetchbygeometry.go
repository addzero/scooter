package scooterlocationfetch

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
)

func (s scooterLocationUsecase) FetchByGeometry(ctx context.Context, point entity.GeoPoint, points []entity.GeoPoint, fetch entity.CommonFetch) ([]entity.ScooterLocation, *uint, error) {
	panic("implement me")
}
