package scooterlocationfetch

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
)

func (s scooterLocationUsecase) FetchByRadius(ctx context.Context, point entity.GeoPoint, radius float64, fetch entity.CommonFetch) ([]entity.ScooterLocation, *uint, error) {
	return s.scooterLocationMongoRepository.FetchByRadius(ctx, point, radius, fetch)
}
