package scooterlocationfetch

import (
	"github.com/addzero/beam/scooter/repository"
	"github.com/addzero/beam/scooter/usecase"
)

type scooterLocationUsecase struct {
	scooterLocationMongoRepository repository.ScooterLocation
}

type ScooterFetchUsecaseParam struct {
	ScooterLocationMongoRepository repository.ScooterLocation
}

func NewScooterFetchUsecase(param ScooterFetchUsecaseParam) usecase.ScooterLocationFetch {
	return &scooterLocationUsecase{
		scooterLocationMongoRepository: param.ScooterLocationMongoRepository,
	}
}
