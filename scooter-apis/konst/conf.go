package konst

const (
	Port          string = "port"
	MongoDbConfig string = "mongodb_config"
	MongoDbName   string = "mongodb_name"
)
