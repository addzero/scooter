package konst

const (
	RequestID                 string = "request_id"
	Scooter                   string = "scooter"
	ScooterID                 string = "scooter_id"
	ScooterLocationCollection string = "scooter_location"
	Limit                     string = "limit"
	Offset                    string = "offset"
	Radius                    string = "radius"
	Latitude                  string = "latitude"
	Longitude                 string = "longitude"
	Desc                      string = "desc"
	Asc                       string = "asc"
)
