package konst

import "fmt"

var (
	RequestIdNotFound = fmt.Errorf("request_id not found")
)
