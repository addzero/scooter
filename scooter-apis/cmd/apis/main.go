package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/addzero/beam/scooter/conf"
	scooterlocationhdr "github.com/addzero/beam/scooter/delivery/http/scooterlocation"
	"github.com/addzero/beam/scooter/konst"
	"github.com/addzero/beam/scooter/repository"
	"github.com/addzero/beam/scooter/repository/scooterlocationmongo"
	"github.com/addzero/beam/scooter/usecase"
	"github.com/addzero/beam/scooter/usecase/scooterlocationfetch"
	"github.com/addzero/beam/scooter/usecase/scooterlocationupsert"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type repositoryPool struct {
	scooterMongo repository.ScooterLocation
}

func newRepositoryPool() repositoryPool {
	scooterMongo := scooterlocationmongo.NewScooterLocationRepository()
	return repositoryPool{
		scooterMongo: scooterMongo,
	}
}

type usecasePool struct {
	scooterUpsert usecase.ScooterLocationUpsert
	scooterFetch  usecase.ScooterLocationFetch
}

func newUsecasePool() usecasePool {
	repoPool := newRepositoryPool()
	scooterUpsert := scooterlocationupsert.NewScooterUpsertUsecase(
		scooterlocationupsert.ScooterUpsertUsecaseParam{
			ScooterRepository: repoPool.scooterMongo,
		})
	scooterFetch := scooterlocationfetch.NewScooterFetchUsecase(
		scooterlocationfetch.ScooterFetchUsecaseParam{
			ScooterLocationMongoRepository: repoPool.scooterMongo,
		})
	return usecasePool{
		scooterUpsert: scooterUpsert,
		scooterFetch:  scooterFetch,
	}
}

func initEndpoint(e *echo.Echo) {
	ucPool := newUsecasePool()
	scooterlocationhdr.NewScooterLocationHandler(scooterlocationhdr.ScooterLocationHandlerParam{
		Echo:                        e,
		ScooterLocationUpserUsecase: ucPool.scooterUpsert,
		ScooterLocationFetchUsecase: ucPool.scooterFetch,
	})
}

func main() {
	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	initEndpoint(e)

	go func() {
		port := int(conf.ReadConfigure(konst.Port, true).(float64))
		if err := e.Start(fmt.Sprintf(":%d", port)); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}
