package validator

import (
	"github.com/go-playground/validator/v10"
)

var instance *validator.Validate

func GetInstance() *validator.Validate {
	return instance
}

func init() {
	if instance == nil {
		instance = validator.New()
	}
}
