package repository

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
)

type ScooterLocation interface {
	Upsert(context.Context, entity.ScooterLocationUpsert) error
	FetchByRadius(context.Context, entity.GeoPoint, float64, entity.CommonFetch) ([]entity.ScooterLocation, *uint, error)
	FetchByGeometry(context.Context, entity.GeoPoint, float64, entity.CommonFetch) ([]entity.ScooterLocation, *uint, error)
}
