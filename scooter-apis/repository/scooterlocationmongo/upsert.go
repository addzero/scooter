package scooterlocationmongo

import (
	"context"
	"fmt"
	"time"

	"github.com/addzero/beam/scooter/entity"
	"github.com/addzero/beam/scooter/konst"
	"github.com/addzero/beam/scooter/service/mongosv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (s scooterLocationRepository) Upsert(ctx context.Context, upsert entity.ScooterLocationUpsert) error {
	scooterLocationCollection := mongosv.Instance().GetCollection(konst.ScooterLocationCollection)

	now := time.Now().UTC()
	opts := options.Update().SetUpsert(true)
	filter := bson.M{"scooter_id": upsert.Scooter.ID}
	update := bson.M{
		"$setOnInsert": bson.M{
			"created_at": now,
		},
		"$set": bson.M{
			"updated_at": now,
			"location": bson.M{
				"type":        "Point",
				"coordinates": []float64{upsert.Location.Longitude, upsert.Location.Latitude},
			},
		},
	}

	upsertResult, upsertErr := scooterLocationCollection.UpdateOne(ctx, filter, update, opts)
	if upsertErr != nil {
		return upsertErr
	} else if upsertResult.ModifiedCount != 1 && upsertResult.UpsertedCount != 1 {
		return fmt.Errorf("fails to upsert game end data")
	}
	return nil
}
