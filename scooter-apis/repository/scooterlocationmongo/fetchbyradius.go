package scooterlocationmongo

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
	"github.com/addzero/beam/scooter/konst"
	"github.com/addzero/beam/scooter/repository/_model"
	"github.com/addzero/beam/scooter/service/mongosv"
	"github.com/jinzhu/copier"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (s scooterLocationRepository) FetchByRadius(ctx context.Context, point entity.GeoPoint, radius float64, fetch entity.CommonFetch) ([]entity.ScooterLocation, *uint, error) {
	scooterLocationCollection := mongosv.Instance().GetCollection(konst.ScooterLocationCollection)
	getNear := bson.D{
		{Key: "$geoNear", Value: bson.M{
			"near": bson.M{
				"type":        "Point",
				"coordinates": []float64{point.Longitude, point.Latitude},
			},
			"maxDistance":   radius,
			"spherical":     true,
			"distanceField": "distance"},
		}}
	limit := bson.D{{Key: "$limit", Value: fetch.Limit}}
	skip := bson.D{{Key: "$skip", Value: fetch.OffSet}}

	cursor, cursorErr := scooterLocationCollection.Aggregate(ctx, mongo.Pipeline{getNear, skip, limit})

	if cursorErr != nil {
		return nil, nil, cursorErr
	}

	var allData []entity.ScooterLocation
	for cursor.Next(ctx) {
		var data _model.ScooterLocation
		decodeErr := cursor.Decode(&data)
		if decodeErr != nil {
			return nil, nil, decodeErr
		}

		var eData entity.ScooterLocation
		copyErr := copier.Copy(&eData, &data)
		if copyErr != nil {
			return nil, nil, copyErr
		}
		allData = append(allData, eData)
	}

	var count *uint
	if fetch.NeedCount {
		cnt := bson.D{{Key: "$count", Value: "count"}}
		countCursor, countErr := scooterLocationCollection.Aggregate(ctx, mongo.Pipeline{getNear, cnt})
		if countErr != nil {
			return nil, nil, countErr
		}
		if countCursor.Next(ctx) {
			var counter _model.Counter
			dcErr := countCursor.Decode(&counter)
			if dcErr != nil {
				return nil, nil, dcErr
			}
			count = &counter.Count
		}
	}

	return allData, count, nil
}
