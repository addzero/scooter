package scooterlocationmongo

import (
	"context"

	"github.com/addzero/beam/scooter/entity"
)

func (s scooterLocationRepository) FetchByGeometry(
	ctx context.Context, point entity.GeoPoint, f float64, fetch entity.CommonFetch) ([]entity.ScooterLocation, *uint, error) {
	panic("implement me")
}
