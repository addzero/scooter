package scooterlocationmongo

import (
	"github.com/addzero/beam/scooter/repository"
)

type scooterLocationRepository struct{}

func NewScooterLocationRepository() repository.ScooterLocation {
	return &scooterLocationRepository{}
}
